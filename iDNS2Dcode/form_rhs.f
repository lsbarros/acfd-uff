      SUBROUTINE form_rhs(nx, ny,                     &
                          stg,                        &
                          rkcna, rkcnb, rkcnc, rkcnd, &
                          d1fx, d1fy, d2fx, d2fy,     &
                          omega, psi,                 &
                          d2x, d2y,                   &
                          nl,                         &
                          rhs,                        &
                          nthreads                    &
                          )
   
      USE                 accuracy
      USE                 parameters
      USE                 omp_lib 
    
      IMPLICIT            none
      
      INTEGER( KIND=ik )  nx,ny
      INTEGER( KIND=ik )  ix,iy
      INTEGER( KIND=ik )  kx,ky
      INTEGER( KIND=ik )  stg
      
      INTEGER( KIND=ik )  nthreads 
      !$ INTEGER( KIND=ik )  my_width, width, my_thread
      INTEGER( KIND=ik )  i_start, i_end       
 
      REAL   ( KIND=rk )  d1fx,d2fx
      REAL   ( KIND=rk )  d1fy,d2fy
      REAL   ( KIND=rk )  D2X ,D2Y
      REAL   ( KIND=rk )  omega,psi
      REAL   ( KIND=rk )  rhs
      REAL   ( KIND=rk )  nl

      REAL   ( KIND=rk )  rkcna(3),rkcnb(3),rkcnc(3),rkcnd(3) 
      
      
      DIMENSION           d1fx(0:nx-1,0:nx-1),d2fx(0:nx-1,0:nx-1)
      DIMENSION           d1fy(0:ny-1,0:ny-1),d2fy(0:ny-1,0:ny-1)
      
      !$ DIMENSION           my_width(nthreads)     

      DIMENSION           omega(0:nx-1,0:ny-1),psi(0:nx-1,0:ny-1)
      DIMENSION           nl(0:nx-1,0:ny-1,2)
      
      DIMENSION           rhs(0:nx-1,0:ny-1)
      DIMENSION           d2x(0:nx-1,0:ny-1)
      DIMENSION           d2y(0:nx-1,0:ny-1)
      

      d2X = MATMUL ( d2fx, omega )
      d2y = MATMUL ( omega, TRANSPOSE( d2fy ) )

       
      
! !$ call omp_set_num_threads(nthreads)

! HERE!! "unroll" the iy-loop
      
!$ call chunk(nthreads,ny,my_width)      

      i_start = 1
      i_end = nx

!$omp parallel private(ix,iy,i_start,i_end,my_thread,width)

!$ my_thread = omp_get_thread_num()
!$ width = my_width(my_thread + 1)     

!$ i_start = 1 + sum(my_width(1:my_thread))
!$ i_end = i_start + width - 1


! !$omp critical
! print*, my_thread, i_start, width
! !$omp end critical               

      DO ix = i_start-1 , i_end-1
         DO iy = 0 , ny - 1
            rhs (ix,iy) = omega (ix,iy)                       &
          + rkcna(stg) * Dt *   ( eins / Re )                 &
          *                     ( D2X (ix,iy) + D2Y (ix,iy) ) &
          + rkcnc(stg) * Dt * nl( ix, iy, 2)                  &
          + rkcnd(stg) * Dt * nl( ix, iy, 1)                  
         END DO
      END DO
 


! !$omp barrier
    
!$omp end parallel
      
      END SUBROUTINE form_rhs
