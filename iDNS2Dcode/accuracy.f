      MODULE accuracy

      INTEGER, PARAMETER, PUBLIC :: ik = SELECTED_REAL_KIND(4)
      INTEGER, PARAMETER, PUBLIC :: rk = SELECTED_REAL_KIND(8)

      REAL( KIND = rk ), PARAMETER, PUBLIC ::     null = 0.0_rk
      REAL( KIND = rk ), PARAMETER, PUBLIC ::     halb = 0.5_rk
      REAL( KIND = rk ), PARAMETER, PUBLIC ::     eins = 1.0_rk
      REAL( KIND = rk ), PARAMETER, PUBLIC ::     zwei = 2.0_rk
      REAL( KIND = rk ), PARAMETER, PUBLIC ::     drei = 3.0_rk
      REAL( KIND = rk ), PARAMETER, PUBLIC ::     vier = 4.0_rk
      REAL( KIND = rk ), PARAMETER, PUBLIC ::    fuenf = 5.0_rk
      REAL( KIND = rk ), PARAMETER, PUBLIC ::    sechs = 6.0_rk
      REAL( KIND = rk ), PARAMETER, PUBLIC ::   sieben = 7.0_rk
      REAL( KIND = rk ), PARAMETER, PUBLIC ::     acht = 8.0_rk
      REAL( KIND = rk ), PARAMETER, PUBLIC ::     neun = 9.0_rk
      REAL( KIND = rk ), PARAMETER, PUBLIC ::     zehn = 1.0e1_rk

      COMPLEX( KIND = rk ), PARAMETER, PUBLIC :: cnull = 0.0_rk
      COMPLEX( KIND = rk ), PARAMETER, PUBLIC :: ceins = 1.0_rk
      COMPLEX( KIND = rk ), PARAMETER, PUBLIC :: czwei = 2.0_rk
      
      END MODULE accuracy
