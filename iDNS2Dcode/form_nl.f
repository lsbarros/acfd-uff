      SUBROUTINE form_nl (nx, ny,                &
                          d1fx, d1fy,            &
                          psi, psix, psiy,       &
                          omega, omegax, omegay, &
                          nl,                    &
                          nthreads)
   
      USE                 accuracy
      USE                 parameters
      USE                 omp_lib
 
      IMPLICIT            none
      
      
      INTEGER( KIND=ik )  nx,ny
      INTEGER( KIND=ik )  ix,iy
     
      
      INTEGER( KIND=ik )  nthreads
      !$ INTEGER( KIND=ik )  my_width, width, my_thread
      INTEGER( KIND=ik )  i_start, i_end
     
 
      REAL   ( KIND=rk )  d1fx
      REAL   ( KIND=rk )  d1fy
      REAL   ( KIND=rk )  omega,psi
      REAL   ( KIND=rk )  psix,psiy,omegax,omegay
      REAL   ( KIND=rk )  nl

      DIMENSION           d1fx(0:nx-1,0:nx-1)
      DIMENSION           d1fy(0:ny-1,0:ny-1)
      DIMENSION           omega (0:nx-1,0:ny-1),psi   (0:nx-1,0:ny-1)
      DIMENSION           psix  (0:nx-1,0:ny-1),psiy  (0:nx-1,0:ny-1)
      DIMENSION           omegax(0:nx-1,0:ny-1),omegay(0:nx-1,0:ny-1)
      DIMENSION           nl(0:nx-1,0:ny-1,2)
      
      !$ DIMENSION           my_width(nthreads) 

!  .. calculate derivatives ..


      psix   = MATMUL ( d1fx, psi )
      omegax = MATMUL ( d1fx, omega )
      psiy   = MATMUL ( psi,   TRANSPOSE( d1fy ) )
      omegay = MATMUL ( omega, TRANSPOSE( d1fy ) )

!  .. calculate the nonlinear term ..

!$ call omp_set_num_threads(nthreads)

! HERE!! "unroll" the iy-loop

!$ call chunk(nthreads,ny,my_width)

      i_start = 1 
      i_end = ny

!$omp parallel private(ix,iy,i_start,i_end,my_thread,width)
 
!$ my_thread = omp_get_thread_num()
!$ width = my_width(my_thread + 1)     

!$ i_start = 1 + sum(my_width(1:my_thread))
!$ i_end = i_start + width - 1

 
      DO iy = i_start, i_end -1
         DO ix = 0, nx -1
            nl ( ix, iy, 2 ) = psix ( ix, iy ) * omegay ( ix, iy ) &
                             - psiy ( ix, iy ) * omegax ( ix, iy )
         ENDDO
      ENDDO
      
!$omp end parallel
 


     END SUBROUTINE form_nl
