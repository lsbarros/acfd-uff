      SUBROUTINE       evdecr                      &
      (                n      , arr    ,           &
                       evs    , p      , pin     , &
                       dwr    , dwi    ,           &
                       dvl    , dvr    ,           &
                       ipivot ,                    &
                       dwork  , zwork              &
      )                                           
                                                  
                                                  
                                                   ! calculates:
                                                   ! the eigenvalue decomposition
                                                   ! of a square dense real
                                                   ! matrix                      arr
                                                  
                                                   ! returns:
                                                   ! the eigenvalues             evs
                                                   ! the (complex) eigenvectors  
                                                   ! in matrix                   p
                                                   ! and the inverse of p        pin

      USE                accuracy
 
      IMPLICIT           none

      INTEGER( KIND=ik ) n
      INTEGER( KIND=ik ) i      , j      , ipivot , info 

      REAL   ( KIND=rk ) arr
      REAL   ( KIND=rk ) dwr    , dwi    , dvl    , dvr    , dwork

      COMPLEX( KIND=rk ) iu     , evs    , p      , pin    , zwork
      
      DIMENSION          ipivot ( 0_ik:n    )
      DIMENSION          evs    ( 0_ik:n    )
      DIMENSION          dwr    ( 0_ik:n    )
      DIMENSION          dwi    ( 0_ik:n    )
      DIMENSION          arr    ( 0_ik:n    , 0_ik:n    )
      DIMENSION          dvl    ( 0_ik:n    , 0_ik:n    )
      DIMENSION          dvr    ( 0_ik:n    , 0_ik:n    )
      DIMENSION          p      ( 0_ik:n    , 0_ik:n    )
      DIMENSION          pin    ( 0_ik:n    , 0_ik:n    )
      DIMENSION          dwork  ( 4_ik   *(n+1_ik))
      DIMENSION          zwork  ( 0_ik   : 2_ik*n+1_ik    )


      iu             = CMPLX ( null , eins )

      info           = 0_ik 
      CALL             DGEEV                               &
      (               'v','v',n+1_ik,arr,n+1_ik,dwr,dwi,         &
                       dvl,n+1_ik,dvr,n+1_ik,dwork,4_ik*(n+1_ik),info  &
      )
      IF             ( info .EQ. 0_ik )THEN
         DO i = 0, n
            evs (i)  = CMPLX ( dwr(i) , dwi(i) )
         END DO
      ELSE
         PRINT*,      'ev decomposition of matrix not calculated'
         STOP
      ENDIF
    
      j = 0
  100 CONTINUE
         IF (ABS(dwi(j)) .LT. 1.e-10_rk ) THEN
            DO i = 0, n
               p ( i , j   ) = dvr ( i ,j )
            END DO 
         ELSEIF  ( dwi(j) .GT. null ) THEN
            DO i = 0, n
               p ( i , j   ) = dvr ( i ,j ) + iu * dvr ( i , j+1 )
               p ( i , j+1 ) = dvr ( i ,j ) - iu * dvr ( i , j+1 )
            END DO
            IF (j+1_ik .GT. n ) GO TO 200
         ENDIF
         j = j+1_ik
         IF ( j .GT. n ) GO TO 200
         GO TO 100
  200 CONTINUE

      DO j = 0, n
         DO i = 0, n
            pin (i,j) = p (i,j)
         END DO
         ipivot (j)   = 0_ik
      END DO
      info=0_ik
      CALL ZGETRF(n+1_ik,n+1_ik,pin,n+1_ik,ipivot,info)
      CALL ZGETRI(n+1_ik,pin,n+1_ik,ipivot,zwork,2_ik*n+1_ik,info)
      
      
      END SUBROUTINE       evdecr                   
