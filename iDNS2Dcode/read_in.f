      SUBROUTINE read_in(nx,ny,omega,psi,t)

      USE                  accuracy

      IMPLICIT             none

      INTEGER( KIND = ik ) nx,ny
      INTEGER( KIND = ik ) ix,iy
      
      REAL   ( KIND = rk ) omega,psi
      REAL   ( KIND = rk ) x,y,t
      REAL   ( KIND = rk ) u, ux, uy, v, vx, vy

      DIMENSION            psi  (0:nx-1,0:ny-1)
      DIMENSION            omega(0:nx-1,0:ny-1)


!   ..open data file..

      OPEN( 11, file = 'DATA.FlowField' )
      
      READ(11,*)t
      DO iy = 0, ny-1
         DO ix = 0, nx-1
            READ(11,'(4(f22.16,1x))')x, y, psi(ix,iy), omega(ix,iy)
!           READ(11,*)x, y, psi(ix,iy), omega(ix,iy), u, ux, uy, v, vx, vy
         END DO
      END DO

      CLOSE(11)
     
      END SUBROUTINE read_in
