module our_lapack
! Purpose: 
!
! some auxiliary linear algebra subroutines
!
! Record of revisions:
!
!    Date       Programmer      Description of change
!    ====       ==========      =====================
! 11/10/2016  L. S. de Barros
!
implicit none

private 
public  my_matmul_ABC_ccrc, my_matmul_ABC_rccc
contains

! +++++++++++++++++++++++++++++++++++++++++++

subroutine my_matmul_ABC_ccrc(mat_result, A, B, C)

implicit none
complex (kind=8), intent(in)  :: A(:,:), C(:,:)
real    (kind=8), intent(in)  :: B(:,:)
complex (kind=8), intent(out) :: mat_result(size(A(:,1)), size(A(1,:)))

mat_result = matmul(A,matmul(B, C))
end subroutine my_matmul_ABC_ccrc
! +++++++++++++++++++++++++++++++++++++++++++

subroutine my_matmul_ABC_rccc(mat_result, A, B, C)

implicit none
complex (kind=8), intent(in)  :: A(:,:), C(:,:)
complex (kind=8), intent(in)  :: B(:,:)
real    (kind=8), intent(out) :: mat_result(size(A(:,1)), size(A(1,:)))

mat_result = matmul(A,matmul(B, C))
end subroutine my_matmul_ABC_rccc
! +++++++++++++++++++++++++++++++++++++++++++

end module our_lapack
