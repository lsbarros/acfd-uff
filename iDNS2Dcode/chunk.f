subroutine chunk(nt,m,my_width)

integer, intent(in):: nt, m
integer, dimension(nt), intent(out):: my_width 


integer:: i,resto,div


resto = mod(m,nt) 
div = int(m/nt)

do i = 1,nt
   if (i.le.resto) then
      my_width(i) = div + 1
    else
      my_width(i) =  div     
   end if
end do

end subroutine chunk
