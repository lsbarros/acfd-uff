#
# script for running and using 'gprof' tool for profiling
#
# instructions:
#
# run with OpenMP:  bash run_profiling.sh -pg -fopenmp
# run serial     :  bash run_profiling.sh -pg
#

# compile and link with <-pg> flag
echo ' '
echo '1. compiling and linking with gprof... '
echo ' '
FFLAGS=$1
FFLAGS2=$2
echo "** using FFLAGS = $FFLAGS and FFLAGS2 = $FFLAGS2"
make all FFLAGS=${FFLAGS} FFLAGS2=${FFLAGS2}

# run the code
echo ' '
./iDNS2d
echo ' '

# run gprof
echo ' '
echo '2. exporting  gprof output... ' 
gprof iDNS2d > profiling.txt
head -15 profiling.txt
rm -v -f gmon.out
echo ' '
