      subroutine       initial_condition(nx,ny,xgrid,ygrid,d1fx,d1fy,omega)

      USE accuracy

      implicit         none

      integer( KIND=ik )             nx , ny
      integer( KIND=ik )             ix , iy
      integer( KIND=ik )             n  , nv
      integer( KIND=ik )             nvx, nvy
      integer( KIND=ik )             kx , ky
     
      character( len = 4)      init_condn
      
      parameter          (init_condn='farg')
      parameter          (nv=3)
 
      real( KIND=rk )                xgrid,ygrid
      real( KIND=rk )                omega
      real( KIND=rk )                pi
      real( KIND=rk )                xn, yn, an
      real( KIND=rk )                r0
      
      real( KIND=rk ), PARAMETER ::  rho = halb / zehn / zehn
      
      real( KIND=rk ), ALLOCATABLE, DIMENSION( :,: ) :: u, v
      
      dimension        xgrid(0:nx-1),ygrid(0:ny-1)
      dimension        xn(nv),yn(nv),an(nv)
      dimension        omega(0:nx-1,0:ny-1)
   
      REAL( KIND = rk ), DIMENSION ( 0:nx-1,0:nx-1 ) :: d1fx
      REAL( KIND = rk ), DIMENSION ( 0:ny-1,0:ny-1 ) :: d1fy
 
      ALLOCATE( u( 0:nx-1,0:ny-1 ), v( 0:nx-1,0:ny-1 ) )  
      
      pi = vier * atan( eins )
      
      r0 = eins / pi



!  .. farge initialization ..

      xn (1) = drei  * pi / vier
      xn (2) = fuenf * pi / vier
      xn (3) = fuenf * pi / vier

      yn (1) = eins * pi
      yn (2) = eins * pi
      yn (3) = eins * pi* ( eins + halb / SQRT( zwei ) )

      an (1) = eins * pi  
      an (2) = eins * pi  
      an (3) =-halb * pi

      do iy = 0, ny-1
         do ix = 0, nx-1
            omega(ix,iy) = null
            do n = 1, nv
               omega(ix,iy) = omega(ix,iy) +           &
               an(n) * exp(-((xgrid(ix) - xn(n)) **2 + &
                             (ygrid(iy) - yn(n)) **2)/ &
                              r0**2)
            enddo
         enddo
      enddo

      
      DEALLOCATE( u, v )
      
      end subroutine initial_condition
