      SUBROUTINE       fourcd                                    &
      (                                                          &
                       nx     , xf , trnsx , gridx , d1fx , d2fx &
      )
!                                                  fourcd:
!                                                  calculate matrices to perform 
!                                                  fourier collocation
!                                                  differentiation in one dimension
      USE              accuracy
      IMPLICIT         none

      INTEGER( KIND=ik )                            :: nx
      INTEGER( KIND=ik )                            :: j, k, l
      
      REAL   ( KIND=rk )                            :: pi, sum
      REAL   ( KIND=rk ), DIMENSION (0:nx-1)        :: xf, trnsx, gridx
      REAL   ( KIND=rk ), DIMENSION (0:nx-1,0:nx-1) :: d1fx, d2fx
     

      pi               =      vier * ATAN( eins )

!                                                  calculate fourier
!                                                  collocation points
      DO j             =      0 , nx-1
         xf(j)         =      zwei * j * pi / nx
      END DO
     
!                                                  calculate fourier
!                                                  derivative matrices

      DO j             =      0 , nx-1 
         DO k          =      0 , nx-1 
            IF                         ( &
                              j .NE. k   &
                                       ) &
                              THEN
                                
               d1fx(j,k)=     (-eins)**(j+k)                    &
                        *     COS(REAL(j-k,KIND=rk) * pi / nx ) &
                        /     SIN(REAL(j-k,KIND=rk) * pi / nx ) &
                        /     zwei
            END IF
         END DO
      END DO
      
      DO j             =       0 , nx-1
         DO k          =       0 , nx-1
            sum        =       null
            DO l       =       0 , nx-1
               sum     =       sum   + d1fx(l,k)*d1fx(j,l)
            ENDDO 
            d2fx(j,k)  =       sum
         ENDDO 
      ENDDO 

!                                                  calculate the grid in the 
!                                                  appropriate periodic direction
      CALL             xgrid                          &
      (                                               &
                       nx     , xf    , trnsx, gridx  &
      )
      
!                                                  modify the derivative matrices:
!                                                  incorporate metrics
      DO j             =       0 , nx-1
         DO k          =       0 , nx-1
            d1fx(j,k)  =       trnsx(j)*d1fx(j,k)
         ENDDO 
      ENDDO 

      DO j             =       0 , nx-1
         DO k          =       0 , nx-1
            sum        =       null
            DO l       =       0 , nx-1
               sum     =       sum   + d1fx(l,k)*d1fx(j,l)
            ENDDO 
            d2fx(j,k)  =       sum
         ENDDO 
      ENDDO 

!                                                  end of subroutine 
!                                                  fourcd
      END SUBROUTINE fourcd
      
      SUBROUTINE       xgrid                          &
      (                                               &
                       nx     , xf    , trnsx, gridx  &
      )

      USE              accuracy

      IMPLICIT         none
      
      INTEGER( KIND=ik )                       :: nx
      INTEGER( KIND=ik )                       :: j      
      
      REAL   ( KIND=rk )  pi     
      REAL   ( KIND=rk )  xmin   , xmax
      REAL   ( KIND=rk ), DIMENSION ( 0:nx-1 ) :: xf
      REAL   ( KIND=rk ), DIMENSION ( 0:nx-1 ) :: gridx, trnsx 
      

      pi             = vier * ATAN( eins )
      xmin           = null
      xmax           = zwei * pi
!     xmin           = -pi
!     xmax           = +pi 
     
!                                                  calculate the streamwise grid

      DO j           = 0 , nx-1
         gridx (j)   = ((xmax - xmin)/zwei/pi) * xf(j) + xmin
      END DO

      DO j           = 0 , nx-1
         trnsx (j)   = zwei * pi / ( xmax - xmin )
      END DO

!                                                  end of subroutine
!                                                  xgrid
      END SUBROUTINE xgrid
