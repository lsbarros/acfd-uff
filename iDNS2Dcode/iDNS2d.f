!      include 'chunk.f90'

      PROGRAM          iDNS2d

! ......................................................................
! ..                                                                  ..
! ..  Direct numerical simulation code                                ..
! ..  for the two-dimensional incompressible Navier-Stokes equations  ..
! ..  in vorticity-streamfunction formulation                         ..
! ..  subject to periodic boundary conditions                         ..
! ..                                                                  ..
! ......................................................................

      USE                            accuracy
      USE                            parameters
      USE                            omp_lib
      USE                            our_lapack, only: my_matmul_ABC_ccrc, my_matmul_ABC_rccc 
      

      IMPLICIT                       none

      INTEGER( KIND=ik )             nx,ny, nthreads, my_width, width, my_thread
      INTEGER( KIND=ik )             n, i_start, i_end
      INTEGER( KIND=ik )             ix,iy,jx,jy
      INTEGER( KIND=ik )             stg
      INTEGER( KIND=ik )             it
      INTEGER( KIND=ik )             ipivot 
      
      REAL   ( KIND=rk )             xgrid,d1fx,d2fx
      REAL   ( KIND=rk )             ygrid,d1fy,d2fy
      REAL   ( KIND=rk )             xf,trnsx,yf,trnsy
      REAL   ( KIND=rk )             omega,psi
      REAL   ( KIND=rk )             U,Ux,Uy,V,Vx,Vy
      REAL   ( KIND=rk )             psix,psiy,omegax,omegay
      REAL   ( KIND=rk )             d2x,d2y
      REAL   ( KIND=rk )             rhs

      REAL   ( KIND=rk )             nl
      REAL   ( KIND=rk )             start,finish
      REAL   ( KIND=rk )             initial_time,current_time
      REAL   ( KIND=rk )             arrx,arry
      REAL   ( KIND=rk )             dwr,dwi,dvl,dvr,dwork,zwork
      REAL   ( KIND=rk )             rkcna(3),rkcnb(3),rkcnc(3),rkcnd(3) 

      COMPLEX( KIND=rk )             csum
      COMPLEX( KIND=rk )             evs_x        
      COMPLEX( KIND=rk )             p_x
      COMPLEX( KIND=rk )             p_inv_x
      COMPLEX( KIND=rk )             evs_y        
      COMPLEX( KIND=rk )             p_y
      COMPLEX( KIND=rk )             p_inv_y
      COMPLEX( KIND=rk )             rhs_hat
      COMPLEX( KIND=rk )             den
      COMPLEX( KIND=rk )             omega_hat,psi_hat
      REAL   ( KIND=rk )             aux,aux_1,aux_2,aux_3
      
      PARAMETER       ( nx = matrix_dim , ny = matrix_dim )
      PARAMETER       ( n = nx-1 , nthreads = 4)
      
      DIMENSION        ipivot ( 0:n    )
      DIMENSION        dwr    ( 0:n    )
      DIMENSION        dwi    ( 0:n    )
      DIMENSION        dvl    ( 0:n    , 0:n    )
      DIMENSION        dvr    ( 0:n    , 0:n    )
      DIMENSION        dwork  ( 4_ik   *(n+1)   )
      DIMENSION        zwork  ( 0_ik:  2_ik*n+1_ik )
       
      DIMENSION        my_width(nthreads)

      DIMENSION        xgrid(0:nx-1),xf(0:nx-1),trnsx(0:nx-1) 
      DIMENSION        ygrid(0:ny-1),yf(0:ny-1),trnsy(0:ny-1) 
      DIMENSION        d1fx(0:nx-1,0:nx-1),d2fx(0:nx-1,0:nx-1)
      DIMENSION        d1fy(0:ny-1,0:ny-1),d2fy(0:ny-1,0:ny-1)
      DIMENSION        evs_x         (0:nx-1)
      DIMENSION        p_x           (0:nx-1,0:nx-1)
      DIMENSION        p_inv_x       (0:nx-1,0:nx-1)
      DIMENSION        evs_y         (0:ny-1)
      DIMENSION        p_y           (0:ny-1,0:ny-1)
      DIMENSION        p_inv_y       (0:ny-1,0:ny-1)
      DIMENSION        arrx          (0:nx-1,0:nx-1)
      DIMENSION        arry          (0:ny-1,0:ny-1)
      DIMENSION        U (0:nx-1,0:nx-1), Ux(0:nx-1,0:nx-1), Uy(0:nx-1,0:nx-1)
      DIMENSION        V (0:nx-1,0:nx-1), Vx(0:nx-1,0:nx-1), Vy(0:nx-1,0:nx-1)
      DIMENSION        rhs  (0:nx-1,0:ny-1),rhs_hat  (0:nx-1,0:ny-1)
      DIMENSION        psi  (0:nx-1,0:ny-1),psi_hat  (0:nx-1,0:ny-1)
      DIMENSION        omega(0:nx-1,0:ny-1),omega_hat(0:nx-1,0:ny-1)
      DIMENSION        psix  (0:nx-1,0:ny-1),psiy  (0:nx-1,0:ny-1)
      DIMENSION        d2x(0:nx-1,0:ny-1)
      DIMENSION        d2y(0:nx-1,0:ny-1)
      DIMENSION        omegax(0:nx-1,0:ny-1),omegay(0:nx-1,0:ny-1)
      DIMENSION        aux  (0:nx-1,0:ny-1)
      DIMENSION        aux_1(0:nx-1,0:ny-1)
      DIMENSION        aux_2(0:nx-1,0:ny-1)
      DIMENSION        aux_3(0:nx-1,0:ny-1)
      DIMENSION        nl(0:nx-1,0:ny-1,2)
      
     
      CHARACTER( LEN =  5 ) c2 
      CHARACTER( LEN = 30 ) c1
      CHARACTER( LEN = 39 ) filename

      LOGICAL          restart
      
      PARAMETER      ( restart = .false. )
      
      
      rkcna(1)= 2.9e1_rk/9.6e1_rk
      rkcna(2)=-3.0e0_rk/4.0e1_rk
      rkcna(3)= 1.0e0_rk/6.0e0_rk 
      
      rkcnb(1)= 3.7e1_rk/1.6e2_rk 
      rkcnb(2)= 5.0e0_rk/2.4e1_rk 
      rkcnb(3)= 1.0e0_rk/6.0e0_rk 
      
      rkcnc(1)= 8.0e0_rk/1.5e1_rk 
      rkcnc(2)= 5.0e0_rk/1.2e1_rk 
      rkcnc(3)= 3.0e0_rk/4.0e0_rk 
      
      rkcnd(1)= null
      rkcnd(2)=-1.7e1_rk/6.0e1_rk 
      rkcnd(3)=-5.0e0_rk/1.2e1_rk 
      
! OMP initialization

!$ call omp_set_num_threads(nthreads)

! !$ print "('Using ', i2 ,' threads')", nthreads

    CALL cpu_time(start)

!$ start = omp_get_wtime()

     !   ..setup calculation grid and differentiation matrix..

      CALL fourcd(nx,xf,trnsx,xgrid,d1fx,d2fx)             

      CALL fourcd(ny,yf,trnsy,ygrid,d1fy,d2fy)

 
!   ..calculate EVD of d2x and d2y ..

      DO iy = 0, nx -1
         DO ix = 0, nx -1
            arrx(ix,iy) = d2fx (ix,iy) 
         END DO
      END DO
      CALL evdecr(nx-1_ik,arrx,evs_x,p_x,p_inv_x,        &
                  dwr,dwi,dvl,dvr,ipivot,dwork,zwork)

      DO iy = 0, ny -1
         DO ix = 0, ny -1
            arry(ix,iy) = d2fy (iy,ix) 
         END DO
      END DO
      CALL evdecr(ny-1_ik,arry,evs_y,p_y,p_inv_y,        &
                  dwr,dwi,dvl,dvr,ipivot,dwork,zwork)
                                                
                                                                     
      IF( restart ) THEN
     

!   ..read in previous-simulation data..
 
         CALL read_in(nx,ny,omega,psi,initial_time)

      ELSE


!   ..calculate the initial condition for omega..
 
         CALL initial_condition(nx,ny,xgrid,ygrid,d1fx,d1fy,omega)
         
         initial_time = null

!   ..output the initial result.. 

         OPEN( 11, file = './RESULTS/DATA.FlowField.Time.00000.dat' )
 
         WRITE(11,*)'VARIABLES = "X", "Y", "Psi", "Omega"'
         WRITE(11,*)'ZONE T= "1",StrandID=1, ', 'SolutionTime=',initial_time
         WRITE(11,*)'I=',nx,', J=',ny,', F= POINT' 
         DO iy = 0, ny-1
            DO ix = 0, nx-1
               WRITE(11,'(4(f22.16,1x))')xgrid(ix), ygrid(iy), &
                                         psi(ix,iy), omega(ix,iy)
            END DO
            WRITE(11,'(4(f22.16,1x))')
         END DO
 
         CLOSE( 11 )

      END IF


!   ..use EVD to calculate the initial psi..


!      omega_hat = MATMUL ( p_inv_x , MATMUL ( omega, p_y ) ) ! matmul 1
      call my_matmul_ABC_ccrc(omega_hat, p_inv_x, omega, p_y)

        
      DO iy = 0, ny-1
         DO ix = 0, nx-1
            
            den            = evs_x(ix) + evs_y(iy)
            IF ( REAL( den ) **2 + AIMAG( den )**2 .lt. 1.e-12_rk) THEN
               psi_hat(ix,iy) = null
            ELSE
               psi_hat(ix,iy) = - omega_hat(ix,iy) / den
            END IF
            
         END DO
      END DO
  
         
!      psi = MATMUL ( p_x , MATMUL ( psi_hat, p_inv_y ) ) ! matmul 2
      call my_matmul_ABC_rccc(psi, p_x, psi_hat, p_inv_y)


!   ..enter the time-integration..

      DO it = 1 , nsteps
 
         current_time = initial_time + REAL ( it, KIND = rk ) * dt
 
         DO iy = 0, ny -1
            DO ix = 0, nx -1
               nl( ix, iy, 1 ) = null
               nl( ix, iy, 2 ) = null
            END DO
         END DO
        
         
         !print*,current_time,omega(nx/2,ny/2)
         
!   ..enter fractional time-stepping..

         DO stg = 1 , 3
 

!   ..form the nl term..

            CALL form_nl (nx,ny,              &
                          d1fx,d1fy,          &
                          psi  ,psix  ,psiy  ,&
                          omega,omegax,omegay,&
                          nl,                 &
                          nthreads)

!   ..form the RHS term..

            CALL form_rhs(nx,ny,                  &
                          stg,                    &
                          rkcna,rkcnb,rkcnc,rkcnd,&
                          d1fx,d1fy,d2fx,d2fy,    &
                          omega,psi,              &
                          d2x,d2y,                &
                          nl,                     &
                          rhs,                    &
                          nthreads                &
                          )
  
  
!  ..perform the time-advancement in EV-space..
            ! --- hello ---
            !rhs_hat = MATMUL ( p_inv_x, MATMUL ( rhs , p_y ) ) ! matmul 3
            call my_matmul_ABC_ccrc(rhs_hat, p_inv_x, rhs, p_y)
      
! HERE!! "unroll" the iy-loop
        !$   call chunk(nthreads,ny,my_width)
  

          i_start = 1
          i_end = ny

!$omp parallel private(ix,iy,i_start,i_end,width,my_thread,den)
          !$  my_thread = omp_get_thread_num()
          !$  width = my_width(my_thread + 1)

        


          !$ i_start = 1 + sum(my_width(1:my_thread)) 
          !$  i_end = i_start + width - 1 
           
! !$omp critical
! print*, my_thread, i_start, width
! !$omp end critical               
          
            DO iy = i_start-1, i_end-1
               DO ix = 0, nx-1
                  omega_hat(ix,iy) = rhs_hat(ix,iy)   /     &
                  (eins - rkcnb(stg) * Dt * evs_x(ix) / Re  &
                        - rkcnb(stg) * Dt * evs_y(iy) / Re )
               END DO
            END DO
 
!         !$omp barrier


! !$omp critical
! print*, my_thread, i_start, width
! !$omp end critical                           

             DO iy = i_start-1, i_end-1
               DO ix = 0, nx-1
                  den            = evs_x(ix) + evs_y(iy)
                  IF ( REAL(den)**2 + AIMAG(den)**2 .LT. 1.e-12_rk) THEN
                     psi_hat(ix,iy) = null
                  ELSE
                     psi_hat(ix,iy) = - omega_hat(ix,iy) / den
                  END IF
               END DO
            END DO
  
!$omp end parallel 


!            omega = MATMUL( p_x, MATMUL ( omega_hat, p_inv_y ) ) ! matmul 4
            call my_matmul_ABC_rccc(omega, p_x, omega_hat, p_inv_y)

!            psi = MATMUL ( p_x , MATMUL ( psi_hat, p_inv_y ) )   ! matmul 5
            call my_matmul_ABC_rccc(psi, p_x, psi_hat, p_inv_y)
 
!  ..update the nonlinear term..
 
            DO iy = 0, ny -1
               DO ix = 0, nx -1
                  nl( ix, iy, 1 ) = nl( ix, iy, 2 )
               END DO
            END DO
        
  
!  ..perform the next time-stage..
    
         END DO


!  ..output results every  stride  time-steps..
    
         IF( ABS(FLOAT(it/stride) - eins*it/stride) .LT. 1.e-6_rk ) THEN
      
            DO iy = 0, ny-1
               DO ix = 0, nx-1
                  U (ix, iy)= DOT_PRODUCT(d1fy (iy , :) , psi (ix, : ))
                  V (ix, iy)=-DOT_PRODUCT(d1fx (ix , :) , psi (: , iy))
               END DO
            END DO
            DO iy = 0, ny-1
               DO ix = 0, nx-1
                  Ux(ix, iy)= DOT_PRODUCT(d1fx (ix , :) , U   (: , iy)) 
                  Uy(ix, iy)= DOT_PRODUCT(d1fy (iy , :) , U   (ix, : )) 
               END DO
            END DO
            DO iy = 0, ny-1
               DO ix = 0, nx-1
                  Vx(ix, iy)= DOT_PRODUCT(d1fx (ix , :) , V   (: , iy))
                  Vy(ix, iy)= DOT_PRODUCT(d1fy (iy , :) , V   (ix, : ))
               END DO
            END DO
               
            CALL integer_2_character(INT(FLOAT(100)*current_time),5,c2)
 
            c1='./RESULTS/DATA.FlowField.Time.'
            filename=c1//c2//'.dat'
            print*,filename

! TecPlot 
! 
!            OPEN(11, FILE = filename, FORM = 'FORMATTED' )
!            WRITE(11,*)'VARIABLES = "X", "Y", "Psi", "Omega", "U ", &
!                       "Ux", "Uy", "V ", "Vx", "Vy"'
!            WRITE(11,*)'ZONE T= "1",StrandID=1, ', &
!                       'SolutionTime=',current_time
!            WRITE(11,*)'I=',nx,', J=',ny,', F= POINT' 
!            DO iy = 0, ny-1
!               DO ix = 0, nx-1
!                  WRITE(11,'(10(f22.16,1x))')xgrid(ix),    &
!                                             ygrid(iy),    &
!                                             psi  (ix,iy), &
!                                             omega(ix,iy), &
!                                             U    (ix,iy), &
!                                             Ux   (ix,iy), &
!                                             Uy   (ix,iy), &
!                                            V    (ix,iy), &
!                                             Vx   (ix,iy), &
!                                             Vy   (ix,iy)
!               END DO
!               WRITE(11,'(10(f22.16,1x))')
!            END DO
!            CLOSE(11)
 
             CALL write_file(nx,ny,filename,current_time,xgrid,ygrid,psi,omega, &
                             U, Ux, Uy, V, Vx, Vy)
        
         END IF


!  ..fractional time-steps completed, proceed to next time-level..

      END DO
 

!   ..output the final result.. 

      OPEN( 11, file = 'DATA.FlowField' )

      write(11,'(f22.16,1x)')current_time
      DO iy = 0, ny-1
         DO ix = 0, nx-1
            write(11,'(4(f22.16,1x))')xgrid(ix), ygrid(iy), &
                                      psi(ix,iy), omega(ix,iy)
         END DO
      END DO

      CLOSE( 11 )


     CALL cpu_time(finish)
!$ finish = omp_get_wtime()


print '("time =",f6.3," seconds.")', finish-start
!   ..end of program iDNS2d..
      
      END PROGRAM iDNS2d
