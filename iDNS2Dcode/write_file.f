      SUBROUTINE write_file(nx,ny,                    &
                            filename,                 &
                            current_time,             &              
                            xgrid,                    &
                            ygrid,                    &
                            psi,                      &
                            omega,                    &
                            U,                        &
                            Ux,                       &
                            Uy,                       &
                            V,                        &
                            Vx,                       &
                            Vy)  

      USE                   accuracy
      USE                   parameters


      IMPLICIT              none

      INTEGER( KIND=ik )    nx,ny
      INTEGER( KIND=ik )    ix,iy
    
      REAL( KIND=rk )       current_time
      REAL( KIND=rk )       xgrid,ygrid,psi,omega
      REAL( KIND=rk )       U,Ux,Uy,V,Vx,Vy

      DIMENSION             xgrid(0:nx-1),ygrid(0:ny-1)
      DIMENSION             psi(0:nx-1,0:ny-1), omega(0:nx-1,0:ny-1)
      DIMENSION     U(0:nx-1,0:nx-1),Ux(0:nx-1,0:nx-1),Uy(0:nx-1,0:nx-1)
      DIMENSION     V(0:nx-1,0:nx-1),Vx(0:nx-1,0:nx-1),Vy(0:nx-1,0:nx-1)
        

      CHARACTER( LEN=39 )   filename  


! TecPlot 
! 
            OPEN(11, FILE = filename, FORM = 'FORMATTED' )
            WRITE(11,*)'VARIABLES = "X", "Y", "Psi", "Omega", "U ", &
                       "Ux", "Uy", "V ", "Vx", "Vy"'
            WRITE(11,*)'ZONE T= "1",StrandID=1, ', &
                       'SolutionTime=',current_time
            WRITE(11,*)'I=',nx,', J=',ny,', F= POINT' 
            DO iy = 0, ny-1
               DO ix = 0, nx-1
                  WRITE(11,'(10(f22.16,1x))')xgrid(ix),    &
                                             ygrid(iy),    &
                                             psi  (ix,iy), &
                                             omega(ix,iy), &
                                             U    (ix,iy), &
                                             Ux   (ix,iy), &
                                             Uy   (ix,iy), &
                                             V    (ix,iy), &
                                             Vx   (ix,iy), &
                                             Vy   (ix,iy)
               END DO
               WRITE(11,'(10(f22.16,1x))')
            END DO
            CLOSE(11)
     
      END SUBROUTINE write_file
