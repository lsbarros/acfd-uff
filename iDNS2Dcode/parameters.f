      MODULE parameters

      USE accuracy

      IMPLICIT NONE

      INTEGER( KIND=ik ), PARAMETER, PUBLIC   :: matrix_dim  = 100_ik  ! resolution in either spatial direction

      INTEGER( KIND=ik ), PARAMETER, PUBLIC   :: nsteps = 401        ! number of time-steps to be performed
      INTEGER( KIND=ik ), PARAMETER, PUBLIC   :: stride = 20           ! data printed every  stride  time-steps

      REAL   ( KIND=rk ), PARAMETER, PUBLIC   :: Re = 1.e3_rk          ! Reynolds number
      REAL   ( KIND=rk ), PARAMETER, PUBLIC   :: dt = 5.e-2_rk         ! time-step

      END MODULE parameters
