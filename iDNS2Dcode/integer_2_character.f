      SUBROUTINE integer_2_character(ii,n,ic)

!  converts integer to character of n positions, complement
!  with zero's before the integer.

      USE           accuracy
      IMPLICIT      NONE

      integer( kind =ik )       ii,n
      integer( kind =ik )       j
      character(len = * ) ic
      

      OPEN(50,status = 'scratch')
      WRITE(50,'(I5)') ii
      REWIND(50)
      READ(50,'(A5)') ic
!      PRINT*,ii,'ic = *',ic
      j = 1
      DO WHILE (ic(j:j) .eq. ' ' .and. j .ne. n)
         ic(j:j) = '0'
         j = j +1
      ENDDO

      CLOSE(50)

!  End of subroutine intToChar

      END SUBROUTINE integer_2_character
