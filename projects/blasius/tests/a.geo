delta = 0.002;
cTM = 0.01;

Point(1) = {cTM*1,0,0};
Point(2) = {cTM*4,0,0};
Point(3) = {cTM*4,2*delta,0};
Point(4) = {cTM*1,2*delta,0};

Line(5) = {1,2};
Line(6) = {2,3};
Line(7) = {3,4};
Line(8) = {4,1};

Line Loop(9) = {5,6,7,8};
Plane Surface(10) = 9;

Transfinite Line{5,6,7,8} = 10;
Transfinite Line{6,-8} = 20 Using Progression 1.1;

Transfinite Surface{10};
Recombine Surface{10};

newEntities[] =
Extrude{0,0,delta/2}
{
	Surface{10};
	Layers{1};
	Recombine;
};

Physical Surface("Inlet")        = {newEntities[{5}]};
Physical Surface("Outlet")       = {newEntities[{3}]}; 
Physical Surface("upWall")       = {newEntities[{4}]};
Physical Surface("downWall")     = {newEntities[2]};
Physical Surface("frontAndBack") = {10, newEntities[0]};
Physical Volume(100)             = {newEntities[1]};
