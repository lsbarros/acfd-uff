// variáveis
cl__1 = 1;
num_point_x = 302;
num_point_y = 102;
refin_x = 1;
refin_y = 1.01;
z = 0.0001;

// pontos
Point(1) = {0.01, 0, 0, 1};
Point(2) = {0.04, 0, 0, 1};
Point(3) = {0.01, 0.004, 0, 1};
Point(4) = {0.04, 0.004, 0, 1};

// linhas (devem estar na ordem ciclica dos pontos)
Line(1) = {1, 2};
Line(2) = {2, 4};
Line(3) = {4, 3};
Line(4) = {3, 1};

// refinamento deve ser determinado individualmente para cada linha
Transfinite Line {1} = num_point_x Using Progression refin_x;
Transfinite Line {3} = num_point_x Using Progression 1/refin_x;
Transfinite Line {2} = num_point_y Using Progression refin_y;
Transfinite Line {4} = num_point_y Using Progression 1/refin_y;

// construir a superfície
Line Loop(6) = {4, 1, 2, 3};
Plane Surface(6) = {6};

// converte elementos triangulares para triangulares/quadrilateros (sempre que possível)
Transfinite Surface {6} = {1,2,3,4};
Recombine Surface {6};

// Estrudar
newEntities[] =
Extrude {0,0,z} 
{  
   Surface{6}; 
   Layers{1};
   Recombine;
}; 

// Nome aos bois
Physical Surface("Back") = {6};
Physical Surface("Front") = {newEntities[0]};
Physical Surface("Left") = {newEntities[2]};
Physical Surface("Bottom") = {newEntities[3]};
Physical Surface("Right") = {newEntities[4]};
Physical Surface("Top") = {newEntities[5]};
Physical Volume(100) = {newEntities[1]};
