# script for gprof test file

# compile with <-pg> flag
gfortran -pg -c linpack_bench.f90 

# link with the <-pg> flag
gfortran -pg linpack_bench.o -o main.exe

# run the code
echo ' ***** code output *****'
echo ' '
./main.exe
echo ' '
echo ' ********************** '

# run gprof
echo ' '
echo ' ***** gprof output ***** ' 
echo ' '
gprof main.exe 
echo ' '
echo ' ************************ '
