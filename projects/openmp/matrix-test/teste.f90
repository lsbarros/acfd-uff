Program teste
Implicit none

integer :: i,j,k,n
real(8) :: start, finish
real(8), dimension(:,:), allocatable :: a,b,c

call cpu_time(start)

n = 1000 

allocate(a(n,n), b(n,n), c(n,n))

do i = 1,n
  do j = 1,n
     a(i,j) = 2.0d0
     b(i,j) = 1.0d0
  end do
end do

a(1,1) = 5.0d0
a(n,n) = 4.0d0
b(1,1) = 2.0d0
b(n,n) = 3.0d0


c = 0.0d0

do k = 1,n
  do j = 1,n
     do i = 1,n
        c(i,j) = c(i,j) + a(i,k)*b(k,j)
     end do
  end do
end do

open(unit=11, File='matrix.dat')
  do i = 1,n
   write(11,*)   (c(i,j), j=1,n)
  end do 
close(11)

call cpu_time(finish)

print '("Time = ",f6.3," seconds.")',finish-start

end Program
