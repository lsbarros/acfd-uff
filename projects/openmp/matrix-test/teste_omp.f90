Program teste_omp


use omp_lib
implicit none

integer :: i,j,k,n, nthreads
real(8) :: start, finish
real(8), dimension(:,:), allocatable :: a,b,c

! Specify number of threads to use:
!$ print *, "How many threads to use? "
!$ read *, nthreads
print *, "How many rows to use (square matrix)? "
read *, n
!$ call omp_set_num_threads(nthreads)
!$ print "('Using OpenMP with ',i3,' threads')", nthreads

call cpu_time(start)
!$ start = omp_get_wtime()

allocate(a(n,n), b(n,n), c(n,n))

do i = 1,n
  do j = 1,n
     a(i,j) = 2.0d0
     b(i,j) = 1.0d0
  end do
end do

a(1,1) = 5.0d0
a(n,n) = 4.0d0
b(1,1) = 2.0d0
b(n,n) = 3.0d0

c = 0.0d0

!$omp parallel do private(i, j, k) 
do k = 1,n
  do j = 1,n
     do i = 1,n
        c(i,j) = c(i,j) + a(i,k)*b(k,j)
     end do
  end do
end do
!$end parallel do

open(unit=11, File='matrix.dat')
  do i = 1,n
   write(11,*)   (c(i,j), j=1,n)
  end do 
close(11)

call cpu_time(finish)
!$ finish = omp_get_wtime()

print '("Time = ", f6.3, " seconds.")',finish - start
end Program
