#
#     -- BLACS example code --
#     University of Tennessee, Knoxville
#
#     Written by Clint Whaley 7/26/94              (rwhaley@cs.utk.edu)
#     Slightly modified by Antoine Petitet 6/5/95  (petitet@cs.utk.edu)
#     Edited for use in instructional labs for JICS by Chris Hastings
#     7/15/96 (hastings@cs.utk.edu)
#     Slightly modified by Osni Marques 9/27/00    (OAMarques@lbl.gov)
#
#     This program performs a simple check-in type hello world. The
#     following routine takes the available processes, forms them into
#     a process grid, and then has each process check in with the
#     process at {0,0} in the process grid.
#
#     .. Parameters ..
      INTEGER            NIN, NOUT
      PARAMETER          ( NIN = 5, NOUT = 6 )
#     ..
#     .. Local Scalars ..
      INTEGER            HISROW, HISCOL, I, IAM, ICALLER, ICONTXT, J,
     $                   NPCOL, NPROCS, NPROW, MYCOL, MYROW
#     ..
#     .. External Subroutines ..
      EXTERNAL           BLACS_GET, BLACS_GRIDINFO, BLACS_GRIDINIT,
     $                   BLACS_PINFO, BLACS_SETUP
#     ..
#     .. External Functions ..
      INTEGER            BLACS_PNUM
      EXTERNAL           BLACS_PNUM
#
#     Determine my process number and the number of processes in
#     machine
#
      CALL BLACS_PINFO( IAM, NPROCS )
#
#     If underlying system needs additional setup, do it now
#
      IF( NPROCS.LT.1 ) THEN
         IF( IAM.EQ.0 ) THEN
            WRITE( NOUT, FMT = 9999 )
            READ( NIN, FMT = * ) NPROCS
         END IF
         CALL BLACS_SETUP( IAM, NPROCS )
      END IF
#
#     Set up process grid that is as close to square as possible
#
      NPROW = INT( SQRT( REAL( NPROCS ) ) )
      NPCOL = NPROCS / NPROW
#
#     Get default system context, and define grid
#
      CALL BLACS_GET( 0, 0, ICONTXT )
      CALL BLACS_GRIDINIT( ICONTXT, 'Row-major', NPROW, NPCOL )
      CALL BLACS_GRIDINFO( ICONTXT, NPROW, NPCOL, MYROW, MYCOL )
#
#     If I'm not in grid, go to end of program
#
      IF( MYROW.EQ.-1 )
     $  GO TO 30
#
#     Get my process ID from my grid coordinates
#
      ICALLER = BLACS_PNUM( ICONTXT, MYROW, MYCOL )
#
#     If I am process {0,0}, receive check-in messages from
#     all nodes
#
      IF( MYROW.EQ.0 .AND. MYCOL.EQ.0 ) THEN
#
         WRITE( NOUT, FMT = * )
         DO 20 I = 0, NPROW-1
            DO 10 J = 0, NPCOL-1
#
               IF( I.NE.0 .OR. J.NE.0 ) THEN
                  CALL IGERV2D( ICONTXT, 1, 1, ICALLER, 1, I, J ) 
               END IF
#
#              Make sure ICALLER is where we think in process grid
#
               CALL BLACS_PCOORD( ICONTXT, ICALLER, HISROW, HISCOL )
               IF( HISROW.NE.I .OR. HISCOL.NE.J ) THEN
                  WRITE( NOUT, FMT = 9996 )
                  STOP
               END IF
               WRITE( NOUT, FMT = 9998 ) I, J, ICALLER
#
   10       CONTINUE
   20    CONTINUE
         WRITE( NOUT, FMT = * )
         WRITE( NOUT, FMT = 9997 )
#
#     All processes but {0,0} send process ID as a check-in
#
      ELSE
         CALL IGESD2D( ICONTXT, 1, 1, ICALLER, 1, 0, 0 )
      END IF
#
   30 CONTINUE
#   
      CALL BLACS_GRIDEXIT( ICONTXT )
      CALL BLACS_EXIT( 0 )
#
 9999 FORMAT( 1X, 'How many processes in machine ?' )
 9998 FORMAT( 1X, 'Hello from process {', I5, ',', I5,
     $        '} (node number =', I12, ') has checked in.' )
 9997 FORMAT( 1X, 'All processes checked in.  Run finished.' )
 9996 FORMAT( 1X, 'Grid error!  Halting . . .' )
#
      STOP
#
      END


