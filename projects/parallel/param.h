integer :: DTYPE_, CTXT_, M_, N_, MB_, NB_, RSRC_
integer :: LLD_, DESC_DIM, BLOCK, CYCLIC, TWO_D_TYPE, CSRC_
      parameter (DTYPE_=1, CTXT_=2, M_=3, N_=4)
      parameter (MB_=5, NB_=6, RSRC_=7, CSRC_=8, LLD_=9)
      parameter (DESC_DIM=9, BLOCK=1, CYCLIC=2, TWO_D_TYPE=1)
integer :: ia, ja, ix, jx, ib, jb, incx, incb, rsrc, csrc
      parameter (ia=1, ja=1, ix=1, jx=1, ib=1, jb=1)
      parameter ( incx=1, incb=1, rsrc=0, csrc=0)
real(8) :: zero, one
      parameter (zero=0.0d0, one=1.0d0)
