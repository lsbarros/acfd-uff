program capitulo3

!   demonstrates the usage of pdgemv, a parallel matrix-vector
!   multiply routine; requires BLACS, MPI, PBLAS libraries
!
      implicit none
      include "param.h"

! parameters
      integer :: nprows, npcols, m, n, nrhs, mb, nb, nbrhs
      parameter (m=9, n=9, nrhs=1, mb=2, nb=2, nbrhs=1)
      integer :: MAXLDA, MAXLDX, MAXSDX
      parameter (MAXLDA=5, MAXLDX=5, MAXSDX=1)
! local arrays
      real(8) :: A(MAXLDA,MAXLDX), x(MAXLDX,MAXSDX), b(MAXLDA,MAXSDX)
      integer :: adesc(DESC_DIM), bdesc(DESC_DIM), xdesc(DESC_DIM)
! local scalars
      integer :: i, j, p, q, myrow, mycol, iam, nprocs, dim1
      integer :: blacs_pnum
      integer :: icontext
      data nprows,npcols/2,3/

! external function
      integer :: numroc
      external numroc

!  set up communication via BLACS

      call blacs_pinfo(iam,nprocs)
      call blacs_get(0,0,icontext)
      call blacs_gridinit(icontext,'r',nprows,npcols)
      
!  initialize using all of the target number of rows and columns

      call blacs_gridinfo(icontext,nprows,npcols,myrow,mycol)

! teste blacs

!if (iam==0) then
!do i=0,(nprows*npcols)-1
!call blacs_pcoord(icontext,i,p,q)
!print *,'Processor ',i,' is at grid position (p,q)=',p,q
!end do
!do p=0,nprows-1
!do q=0,npcols-1
!print *,'At grid position (',p,',',q,') is processor',&
!blacs_pnum(icontext,p,q)
!end do
!end do
!end if

!  define the "desc" parameter for arrays A, x, and b

      call desc_setup(adesc,m,n,mb,nb,icontext,MAXLDA)
      call desc_setup(xdesc,n,nrhs,nb,nbrhs,icontext,MAXLDX)
      call desc_setup(bdesc,m,nrhs,mb,nbrhs,icontext,MAXLDA)

! generate matrices A and x
 
     call genmat(adesc,A,xdesc,x,myrow,mycol,MAXLDA,MAXLDX)

! matrix-vector operation b = Ax (x return from pdgemv as b)
call pdgemv('N',m,n,one,a,ia,ja,adesc,x,ix,jx,xdesc,incx, &
     zero,b,ib,jb,bdesc,incb)

! write solution x to screen
      if(mycol.eq.0) then
        dim1 = max(1,numroc(m, mb, myrow, 0, nprows))
        write(*,"('For process row',i3,':',(5f8.1))")myrow, &
        (b(i,1),i=1,dim1)
      endif

      call blacs_gridexit(icontext)
      call blacs_exit(0)

     
end program

subroutine desc_setup(Adesc,m,n,mb,nb,icontext,LDA)
      implicit none
      include "param.h"
      integer m, n, mb, nb, icontext, LDA, Adesc(DESC_DIM)

      Adesc(DTYPE_) = 1
      Adesc(M_) = m
      Adesc(N_) = n
      Adesc(MB_) = mb
      Adesc(NB_) = nb
      Adesc(RSRC_) = rsrc
      Adesc(CSRC_) = csrc
      Adesc(CTXT_) = icontext


      Adesc(LLD_) = LDA

      return
end

subroutine genmat(descA,A,descB,B,myrow,mycol,LDA,LDB)
      implicit none
      include "param.h"
      integer i, j, mb, nb, myrow, mycol, LDA, LDB
      integer descA(DESC_DIM), descB(DESC_DIM)
      double precision A(LDA, *), B(LDB,*)

      if (myrow.eq.0 .and. mycol.eq.0) then
        A(1,1) = 11
        A(1,2) = 12
        A(1,3) = 17
        A(1,4) = 18
        A(2,1) = 21
        A(2,2) = 22
        A(2,3) = 27
        A(2,4) = 28
        A(3,1) = 51
        A(3,2) = 52
        A(3,3) = 57
        A(3,4) = 58
        A(4,1) = 61
        A(4,2) = 62
        A(4,3) = 67
        A(4,4) = 68
        A(5,1) = 91
        A(5,2) = 92
        A(5,3) = 97
        A(5,4) = 98
        B(1,1) = 1
        B(2,1) = 2
        B(3,1) = 5
        B(4,1) = 6
        B(5,1) = 9
      else if (myrow.eq.0 .and. mycol.eq.1) then
        A(1,1) = 13
        A(1,2) = 14
        A(1,3) = 19
        A(2,1) = 23
        A(2,2) = 24
        A(2,3) = 29
        A(3,1) = 53
        A(3,2) = 54
        A(3,3) = 59
        A(4,1) = 63
        A(4,2) = 64
        A(4,3) = 69
        A(5,1) = 93
        A(5,2) = 94
        A(5,3) = 99
      else if (myrow.eq.0 .and. mycol.eq.2) then
        A(1,1) = 15
        A(1,2) = 16
        A(2,1) = 25
        A(2,2) = 26
        A(3,1) = 55
        A(3,2) = 56
        A(4,1) = 65
        A(4,2) = 66
        A(5,1) = 95
        A(5,2) = 96
      else if (myrow.eq.1 .and. mycol.eq.0) then
        A(1,1) = 31
        A(1,2) = 32
        A(1,3) = 37
        A(1,4) = 38
        A(2,1) = 41
        A(2,2) = 42
        A(2,3) = 47
        A(2,4) = 48
        A(3,1) = 71
        A(3,2) = 72
        A(3,3) = 77
        A(3,4) = 78
        A(4,1) = 81
        A(4,2) = 82
        A(4,3) = 87
        A(4,4) = 88
        B(1,1) = 3
        B(2,1) = 4
        B(3,1) = 7
        B(4,1) = 8
      else if (myrow.eq.1 .and. mycol.eq.1) then
        A(1,1) = 33
        A(1,2) = 34
        A(1,3) = 39
        A(2,1) = 43
        A(2,2) = 44
        A(2,3) = 49
        A(3,1) = 73
        A(3,2) = 74
        A(3,3) = 79
        A(4,1) = 83
        A(4,2) = 84
        A(4,3) = 89
      else if (myrow.eq.1 .and. mycol.eq.2) then
        A(1,1) = 35
        A(1,2) = 36
        A(2,1) = 45
        A(2,2) = 46
        A(3,1) = 75
        A(3,2) = 76
        A(4,1) = 85
        A(4,2) = 86
      endif

      return
end
