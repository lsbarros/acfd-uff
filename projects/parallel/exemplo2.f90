program exemplo2
implicit none

! How to set up a processor grid and perform data distribution
! over it for a matrix-vector multiplication program A x b
! where A = 16x16 and b = 16x1 filled with random integer 

integer :: ictxt,myid,nprocs,nprow,npcol
integer :: p,q,myrow,mycol, n=16
integer :: blacs_pnum

integer :: a(n,n), b(n)

a(:,:) = 0
b(:) = 0

! First we must decide the processr grid: on here 4 processors
! arranged on a 2x2 processor grid

! Initializa the BLACS library BLACS_PINFO and BLACS_GET
call blacs_pinfo(rank,nprocs)
call blacs_get(-1,0,ictxt)
!nprow = int(sqrt(real(nprocs)))
!npcol = nprocs/nprow
nprow = 2
npcol = 2

! BLACS_GRIDINT and BLACS_GRIDINFO to create the processor grid
! using specific rows and columns
call blacs_gridinit(ictxt,'r',nprow,npcol)
call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

! The desired Blocking Factor (BF) for A and b
! we choose BF = 8x8 for A and BF = 8x1 for b
! this "quadrant" type is quite common since it gives equal 
! amounts of array elements to the local memory of each processor

! Building the array descriptor vector: for the matrix A first
! then the vector b, and finally the vector y (argument in PBLAS
! that returns the result of the multiplication)

! Matrix A
m = 16
n = 16
mb = 8
nb = 8
rsrc = 0
csrc = 0
llda = 8
call descinit(desca,m,n,mb,nb,rsrc,csrc,ictxt,llda,info)

!Vector b and y
n = 1
nb = 1
lldb = 8
call descinit(descb,m,n,mb,nb,rsrc,csrc,ictxt,lldb,info)
call descinit(descy,m,n,mb,nb,rsrc,csrc,ictxt,lldy,info) 

! The local array A (la) and the local array b (lb) must be
! filled with the appropriate subarrays of A. At this point the
! parralel matrix-vector PBLAS subroutine ca be called.

open(unit=12, file="a.dat")
write(12,*) = a
open(unit=13, file="b.dat")
write(13,*) = b

if (myrow.eq.0.and.mycol.eq.0) then
  do i = 1,8
    do j = 1,8
      la(i,j) = a(i,j)
    end do
     lb(i) = b(i)
   end do
end if

if (myrow.eq.1.and.mycol.eq.0) then
  do i = 1,8                        
    do j = 1,8                     
      la(i,j) = a(i+llda,j)               
    end do                         
     lb(i) = b(i+lldb)
   end do
end if

if (myrow.eq.0.and.mycol.eq.1) then
  do i = 1,8                       
    do j = 1,8                     
      la(i,j) = a(i,j+llda)               
    end do                            
   end do          
end if                                      
                                   
if (myrow.eq.1.and.mycol.eq.1) then
  do i = 1,8
    do j = 1,8
      la(i,j) = a(i+llda,j+llda)
    end do
   end do
end if      
    
call blacs_gridexit(ictxt)
call blacs_exit(0)
end program exemplo2
