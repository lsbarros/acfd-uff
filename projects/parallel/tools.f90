program tools
integer ictxt,myid,nprocs,nprow,npcol
integer p,q,myrow,mycol
integer blacs_pnum
call blacs_pinfo(myid,nprocs)
call blacs_get(-1,0,ictxt)
! Calculations to make the most square-like proc grid possible
nprow=int(sqrt(real(nprocs)))
npcol=nprocs/nprow
call blacs_gridinit(ictxt,'r',nprow,npcol)
call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)
if (myid==0) then
do i=0,(nprow*npcol)-1
call blacs_pcoord(ictxt,i,p,q)
print *,'Processor ',i,' is at grid position (p,q)=',p,q
end do
do p=0,nprow-1
do q=0,npcol-1
print *,'At grid position (',p,',',q,') is processor',&
blacs_pnum(ictxt,p,q)
end do
end do
end if
call blacs_gridexit(ictxt)
call blacs_exit(0)
end program tools
