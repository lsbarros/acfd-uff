# Course: Advanced Computational Fluid Dynamics #
## Coordinator: Vassilis Theofilis ##
## Academic Year: 2016-17 ##

Length: 

* 60h (48h classroom and 12h tutorials) 

* 180h total student time

**Objectives:** 
Extend knowledge on prototype (hyperbolic, parabolic, elliptic) equations acquired in previous courses to the equations of Fluid Mechanics. Write, debug and run:
Two incompressible NS codes for the solution of two-dimensional problems, with:
A splitting scheme in primitive variables, using finite-difference spatial discretization
Solution of the vorticity transport equation, using spectral collocation spatial discretization
Two compressible NS codes for the solution of one- and two-dimensional problems
Burgers equation in two spatial dimensions
Interaction of a vortex with a shock
Familiarization with three CFD open source codes, OpenFOAM (finite volumes), nek5000 (spectral element, incompressible) and nektar++ (spectral element, incompressible and Discontinuous Galerkin, compressible), including appropriate meshing s/w
Learn basic parallelization techniques using OpenMP (UFF) and MPICH2 (UPM). Apply to:
Solve large linear systems of equations, AX=B, by distributing matrix A
Parallelize own existing codes, identifying performance bottlenecks

**Attendees:**

* Heitor Barros heitorhb@yahoo.com.br
* Hélio Quintanilha helioricardo@id.uff.br
* Jaguarê Smith jaguar_gonzaga@hotmail.com
* Leandro de Barros lsbarros@id.uff.br
* Nelson Braga nelsonbrj@gmail.com
* Pedro Vayssière  pedrovayssiere@id.uff.br
* Ricardo Dias   ricardoiprj@gmail.com
* Vítor José  vitor_moraes@id.uff.br

**Schedule:**
Taught Classes:
Aug 29 – Sept 2 (UFF): 
Parallelization (6h), Introduction to OpenFOAM (6h)
Sept 26 – 28 (UFF): 
Incompressible NS: Splitting method (6h), Vorticity transport (6hrs)
Nov 4 – 11 (Vitoria, before/during/after ENCIT 2016): 
Nek5000 (6h), nektar++ (6hrs)
Week 47 (UFF): Compressible NS: 2D Burgers equation (6hrs), Vortex/shock interaction (6h)

Online Tutorials (Skype name: v.theofilis):

* Sept 14: 2pm – 6 pm (Central European Time)
* Week 41: 4h
* Week 45: 4h 
* Week 49: 4h

Assessment:
Two individual and two group assignments for each student

Index of Projects (to be filled in by the alumni):
IP-01: ….
IP-02: ….
GP-01: Solution of A X = B using MPI and the ScaLAPACK library
GP-02: …

Project suggestions:
Use OpenFOAM to study:
Laminar Blasius boundary layer on a flat plate
Laminar, incompressible, 2D backward-facing step flow 
in a closed domain
in an open domain
Laminar, incompressible, 2D sudden expansion flow
Laminar, supersonic, 2D forward-facing step flow
inviscid
viscous 
Laminar, incompressible, 2D flow over a cylinder
Re = 25 (25) 150
Laminar, incompressible, 2D flow over an airfoil
Symmetric: NACA-0015, at 0 < AoA < 20
Cambered: NACA-4415, at 0 < AoA < 20
Turbulent, transonic, 2D flow over a lifting airfoil
Deadlines:
Friday, November 18: GP-01 and odd Individual Projects (IP-01, IP-03,…)
Friday, December 16: GP-02, GP-03 and even Individual Projects (IP-02, IP-04,…)

Assets: 
Student’s own laptop
Computing cluster at UFF (OpenMP)
One node of the computing cluster at Univ. Politécnica de Madrid (OpenMP and MPICH2)
(64 CPUs, 512 TB shared memory)
pre-installed OpenFOAM, nek5000 and nektar++ software
Open source PBS (Torque) job scheduling system

## Bibliography: ##

* R. H. Pletcher, J. C. Tannehill, D. A. Anderson (2013) “Computational Fluid Mechanics and Heat Transfer”, (3rd Edition), CRC Press
* Online Documentation

* [OpenFOAM](http://www.openfoam.com)

* [nek5000](https://nek5000.mcs.anl.gov/)

* [nektar++](http://www.nektar.info/)

* Material distributed in class